﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using MySql.Data.MySqlClient;

namespace Mais_ian_backend
{
    public static class WebApiConfig
    {
        public static MySqlConnection conn()
        {
            String connectionString = "server=10.0.2.252;port=3306;database=MaidApp;username=MaidApp;password=Gowild@000";
            MySqlConnection conn = new MySqlConnection(connectionString);
            return conn;
        }
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}

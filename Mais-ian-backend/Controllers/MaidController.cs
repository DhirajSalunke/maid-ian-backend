﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace Mais_ian_backend.Controllers
{
    public class Maid{
        public int id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string slot1 { get; set; }
        public string slot2 { get; set; }
        public string slot3 { get; set; }
    }
    public class MaidController : ApiController
    {
        public List<Maid> maidList = new List<Maid>();
        // GET api/values
        public string Get()
        {
            Maid maid = new Maid();
            MySqlConnection conn = WebApiConfig.conn();
            string GetMaidsQuery = "select * from Maids";
            MySqlCommand mySqlCommand = conn.CreateCommand();
            mySqlCommand.CommandText = GetMaidsQuery;
            try
            {
                conn.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                return "Failed";
            }
            MySqlDataReader fetchQuery = mySqlCommand.ExecuteReader();
            while (fetchQuery.Read())
            {
                maid.id = Convert.ToInt32(fetchQuery["MaidId"]);
                maid.name = fetchQuery["Name"].ToString();
                maid.email = fetchQuery["Email"].ToString();
                maid.type = fetchQuery["MaidType"].ToString();
                maid.phone = fetchQuery["Phone"].ToString();
                maid.slot1 = fetchQuery["slot1"].ToString();
                maid.slot1 = fetchQuery["slot1"].ToString();
                maid.slot1 = fetchQuery["slot1"].ToString();
                maidList.Add(maid);
            }
            conn.Close();
            return JsonConvert.SerializeObject(maidList);
        }

        // GET api/values/5
        public string Get(int id)
        {
            Maid maid = new Maid();
            MySqlConnection conn = WebApiConfig.conn();
            string getMaidWithIdQuery = "select * from Users where MaidId like '" + id.ToString() + "'";
            MySqlCommand mySqlCommand = conn.CreateCommand();
            mySqlCommand.CommandText = getMaidWithIdQuery;
            try
            {
                conn.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                return "retVal";
            }
            MySqlDataReader fetchQuery = mySqlCommand.ExecuteReader();
            while (fetchQuery.Read())
            {
                maid.id = Convert.ToInt32(fetchQuery["MaidId"]);
                maid.name = fetchQuery["Name"].ToString();
                maid.email = fetchQuery["Email"].ToString();
                maid.type = fetchQuery["MaidType"].ToString();
                maid.phone = fetchQuery["Phone"].ToString();
                maid.slot1 = fetchQuery["slot1"].ToString();
                maid.slot1 = fetchQuery["slot1"].ToString();
                maid.slot1 = fetchQuery["slot1"].ToString();
            }
            conn.Close();
            return JsonConvert.SerializeObject(maid);
        }

        [Route("api/Maid/{id}/{slot}/{user}")]
        [HttpPut]
        public void Put(int id, int slot, string user)
        {
            MySqlConnection conn = WebApiConfig.conn();
            string updateBookedStatusQuery = "UPDATE Maids SET slot" + slot.ToString() +" = 'Booked' WHERE MaidId = " + id.ToString();
            MySqlCommand mySqlCommand = conn.CreateCommand();
            mySqlCommand.CommandText = updateBookedStatusQuery;
            try
            {
                conn.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
            }
            MySqlDataReader fetchQuery = mySqlCommand.ExecuteReader();
            fetchQuery.Read();
            conn.Close();
            string updateUserQuery = "UPDATE Users SET MaidId = " + id.ToString() + "WHERE UserEmail = '" + user + "'";
            MySqlCommand mySqlCommandForUser = conn.CreateCommand();
            mySqlCommand.CommandText = updateUserQuery;
            try
            {
                conn.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
            }
            MySqlDataReader fetchQueryOfUser = mySqlCommand.ExecuteReader();
            fetchQuery.Read();
            conn.Close();
            //Email user.
        }
    }
}
